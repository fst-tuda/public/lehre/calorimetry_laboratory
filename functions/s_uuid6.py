import uuid6

# Generate a new UUID using the uuid6 library
my_uuid = uuid6.uuid6()
# Print the generated UUID to the console
print(my_uuid)
# Check (assert) that the previously generated UUIDv6 is less than a newly generated one
# This is based on the fact that UUIDv6 has a timestamp component, making newer UUIDs
# larger in value. If the condition is not met, the program will raise an
# AssertionError.
assert my_uuid < uuid6.uuid6()
