from typing import Dict, List, Tuple, Union

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import NDArray


def compare_start_time(
    dataset_start_time: float, experiment_start_time: Union[float, None]
) -> float:
    """Calculates the start time of the entire experiment. In order to plot data
    relative to this timestamp.

    The currently assumed start time (`experiment_start_time`) is compared with the
    start time of data set (`dataset_start_time`). If `dataset_start_time` is smaller
    than `experiment_start_time`, the `experiment_start_time` is updated.

    In case the `experiment_start_time` is None, it is set to `dataset_start_time`.

    Args:
        dataset_start_time (float): Start timestamp of a data set.
        experiment_start_time (Union[float, None]): Assumed start timestamp of the
            experiment, either float or None.

    Returns:
        float: New assumed start timestamp of the experiment.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def get_measdata(
    group: h5.Group,
    data_structure: Dict[str, List[Union[str, NDArray]]],
    length: int,
    start_time: float,
) -> Dict[str, List[Union[str, NDArray]]]:
    """Adds data from an HDF5 group into a data structure.

    This function extracts temperature and timestamp data from an HDF5 group and also
    extracts the sensor name metadata. The data is saved into a dictionary.

    The dictionary consists of three keys: `temperature`, `time` and `sensor_name`. The
    temperature and timestamp data is appended to a list at the corresponding key. Both
    temperature and timestamp data are extracted from `group` as NDArrays. The length of
    the arrays is adjusted with `length` relative to the `start_time`. `length` gives
    the number of data points counted from the start of the array. Furthermore the time
    data is adjusted for `start_time`, meaning `start_time` is substracted from the
    timestamps.

    The `name` metadata from the HDF5 group is stored at the `name` key of the return
    dictionary.

    Args:
        group (h5.Group): HDF5 group containing data sets.
        data_structure (Dict[str, List[NDArray]]): Data structure to append data to.
        length (int): Number of data points to extract.
        start_time (float): Start timestamp of the experiment.

    Returns:
        Dict[str, List[NDArray]]: Updated data structure.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def convert_to_ndarray(data: Dict[str, List]) -> Dict[str, NDArray]:
    """Converts values of a dictionary from lists to NDArrays.

    Args:
        data (Dict[str, List]): A dictionary where the values are lists.

    Returns:
        Dict[str, NDArray]: A dictionary where the values are NDArrays.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def cal_mean_and_standard_deviation(data: NDArray) -> NDArray:
    """Calculates mean and standard deviation for raw data of multiple sensors.

    It is assumed that the raw data is stored in a NDArray with the shape (m, n). m
    stands for the number of sensors and n is the number of measuring points. The first
    row of the resulting NDArray contains the mean value of all the measurment values
    with the same timestamp, the second row the standard deviation.

    Args:
        data (NDArray): Raw data with shape (m, n).

    Returns:
        NDArray: Mean and standard deviation in a 2D NDArray with shape (2, n).

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def plot_errorbar(
    axes: plt.Axes, temperature: NDArray, time: NDArray, legend: str, marker: str
) -> None:
    """Plots an errorbar plot on the given axes.

    This function takes two NDArrays for temperature data (y axis) and time data (x
    axis). The temperature data should be in a 2D NDArray with shape (2, n), where n is
    the number of data points. The first row contains the mean values, and the second
    row contains the standard deviations of the temperature data. The time data is in a
    1D NDArray.

    Args:
        axes (plt.Axes): Object of matplotlib axes.
        temperature (NDArray): 2D NDArray containing the temperature data.
        time (NDArray): 1D NDArray containing the timestamps.
        legend (str): Label for the data points.
        marker (str): Data point marker, e.g. 'o' for circle (see matplotlib.markers).

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def set_labels(axes: plt.Axes, x_label: str, y_label: str) -> None:
    """Sets the axis labels and legend for the axes.

    This function sets the axis labels and legend for the given matplotlib axes.

    Args:
        axes (plt.Axes): Object of matplotlib axes.
        x_label (str): Label for x-axis.
        y_label (str): Label for y-axis.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def get_start_end_temperature(
    temperature_data: NDArray, threshold: float = 0.05
) -> Tuple[float, float]:
    """Calculates the high and low temperatures from a data set.

    This function computes the (average of) the highest temperatures and the (average
    of) the lowest temperatures within a given threshold from the maximum and minimum
    temperatures recorded in the data set. These are considered as the ending and
    starting temperatures respectively.

    Args:
        temperature_data (NDArray): Temperature data set as a 2D NDArray.
        threshold (float): The threshold used to identify temperatures close
            to the maximum and minimum values as high and low temperatures respectively.
            Default to 0.05.

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and
            the average low temperature second.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def get_plot_data_from_hdf5(file_path: str, group_path: str) -> Dict[str, NDArray]:
    """Gets plot data from a group in an HDF5 file.

    This function reads a group of an HDF5 file which contains data of sensors. It
    returns a predefined data structure which is a dictionary with three keys:
    `temperature`, `time` and `sensor_name`. The values are NDArrays of floats or
    strings containing data and labels for the plot.

    Args:
        file_path (str): Path to an HDF5 file.
        group_path (str): Path in HDF5 to a group.

    Returns:
        Dict[str, NDArray]: Data for plot.

    """
    mess_data = {"temperature": [], "time": [], "name": []}

    with h5.File(file_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                dataset_len = len(group[subgroup]["timestamp"])

                # Find the minimum length of the data set.
                if min_len is None or dataset_len < min_len:
                    min_len = dataset_len

                subgroups.append(subgroup)

            # Only group with a data set called timestamp will be read.
            except KeyError:
                continue

            start_time = compare_start_time(dataset_start_time, start_time)

        for subgroup in subgroups:
            mess_data = get_measdata(group[subgroup], mess_data, min_len, start_time)

    output = convert_to_ndarray(mess_data)

    return output


def plot_temperature_over_time(
    temperature: List[NDArray],
    time: List[NDArray],
    legend: List[str],
    x_label: str,
    y_label: str,
) -> None:
    """Plots temperature from multiple sources over time.

    This function creates a plot representing temperature changes over time. Standard
    deviation of the data is visualized with error bars.

     Args:
        temperature (List[NDArray]): A list of NDArray where each NDArray represents
            the temperature data (1st row: mean values, 2nd row: standard deviation) of
            one heat source. Each NDArray should have a shape of (2, n), with n
            representing the number of data points.
        time (List[NDArray]): A list of NDArray with timestamps. Each NDArray should
            have a shape of (n, ).
        legend (List[str]): A list of labels for plot.
        x_label (str): The label for the x axis.
        y_label (str): The label for the y axis.

    """
    _, ax = plt.subplots(1, 1)
    markers = ["o", "^", "2", "p", "D"]

    for i in range(len(temperature)):
        plot_errorbar(ax, temperature[i], time[i], legend[i], markers[i])

    set_labels(ax, x_label, y_label)
    ax.ticklabel_format(scilimits=(0, 3))
