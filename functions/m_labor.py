import json
from typing import Dict

import h5py as h5


def write_heater_data(file_path: str, heater_data: Dict[str, Dict[str, float]]) -> None:
    """Writes the heater data into an existing HDF5 file.

    This function saves the heater data, which is in a dictionary, into an HDF5 file.
    The keys of the dictionary are the UUIDs of heaters and the values are themselves
    dictionary containing the current, voltage, and heating time measurements under the
    keys `Current in Ampere`, `Voltage in Volt`, and `Heat time in Seconds`. The code
    also works as intended if only one heater UUID is used.

    For each UUID in the heater data dictionary, a subgroup with the UUID as name is
    created in the `RawData` group of the HDF5 file. A data set is created to store the
    value for each measurement (named `current`, `voltage` or `heat_time`) in the
    associated subgroup. Each measurment is assigned the metadata attribute `unit`,
    which contains the value's unit.

    Args:
        file_path (str): Path to an HDF5 file.
        heater_data (Dict[str, Dict[str, float]]): A dictionary of heater data.

    """
    raise NotImplementedError(
        "Delete these 3 lines if you have finished the code or want to test it.".upper()
    )


def logging_heater(file_path: str, uuid: str) -> Dict[str, Dict[str, float]]:
    """Prompts the user to enter heater data and log it into an HDF5 file.

    This function creates a subgroup in an HDF5 file with heater's UUID and prompts the
    user to enter current, voltage, and heating time. It validates the user's input and
    writes the input into a group in the HDF5 file. Each piece of data is stored
    in a separate data set with a corresponding unit attribute.

    Args:
        file_path (str): Path to an HDF5 file where data should be logged.
        uuid (str): UUID of a heater.

    Returns:
        Dict[str, Dict[str, float]]: A dictionary containing input data organized by
        UUID and measurement type.

    """
    h5_path = "RawData/{}".format(uuid)

    with h5.File(file_path, "a") as f:
        f.create_group(h5_path)

    data_dict = {
        uuid: {
            "Current in Ampere": None,
            "Voltage in Volt": None,
            "Heat time in Seconds": None,
        }
    }

    need_input = True
    while need_input:
        for k in data_dict[uuid]:
            while data_dict[uuid][k] is None:
                try:
                    data_dict[uuid][k] = float(input("{} = ".format(k)))
                except ValueError:
                    print("[ERROR] Invalid input, please try again.\n")
                    data_dict[uuid][k] = None
                    continue

        print(json.dumps(data_dict, indent=4))
        user_confirm = None
        while user_confirm not in ("y", "n", "Y", "N"):
            user_confirm = input("Is the data correct? (y/n)\n")
        if user_confirm in ("y", "Y"):
            need_input = False
        else:
            for k in data_dict[uuid]:
                data_dict[uuid][k] = None
            print()

    write_heater_data(file_path, data_dict)

    return data_dict
